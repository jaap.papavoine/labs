# Gitlab and git remote exercies

## Exercise 1: Create gitlab.com account

For the exercises, we will be using the public gitlab. Please create an account on gitlab.com.

## Exercise 2: Try to find the labs project

In this exercise we will try to find the labs project. My username is jaap.papavoine. 
The public GitLab Search is notoriously slow, so here is a direct link:
<https://gitlab.com/jaap.papavoine/labs>

Notice how you can browse the git repository directly from the webui.
Also Note that you can view markdown files (.md) as nicely formatted pages. Another reason to use more .md files!

## Exercise 3: Create your first repository

Try and create your first project in GitLab. If you do not yet understand any of the options, please just leave them default.

## Exercise 4: Clone your first repository

1. go to your cdtraining directory.
2. Make a new clone of the repository you created in Exercise 3.
    - `git clone https://gitlab.com/username/projectname.git`
If you need assistance, you can open the webui in gitlab for instructions.

Bonus:
Try and find out what remotes you have.
    - `git remote` or `git remote -v`


## Exercise 5: Push new content to your remote repository

In this exercise we will add new content to your previously created repository.

1. CD into your newly cloned repository from Exercise 4.
2. Check to see if you are on master. If not, checkout master.
    - `git checkout master`
2. Create a new commit. You should now how to do this by now.
3. Push it to the remote.
    - `git push`
4. Open the gitlab WebUI and find your change in your repository.

The `git push` command works, because it will default to the current branch and it's remote branch. master already existed remotely. If you would try `git push` without any other options on a different branch it would fail if the branch does not already exists remotely.

## Exercise 6: Push a branch

In this exercise we will try to push a certain branch we have locally, but NOT push a different local branch.
1. Create two branches, each with unique commits in them (for example )
2. Now push one branch
    - `git push origin <branchname>`
3. Check using the webui to see if your branch is there. Also check if the other branch is NOT there.
4. Try and set a tag on the other branch
    - `git tag <tagname> <otherbranchname>`
5. Push the other branch.
6. Is the tag in the remote? Check using the webui.

Bonus: Try if you can push the tag.