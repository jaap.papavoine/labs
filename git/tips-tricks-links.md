# Tips, trics and usefull resources

## Tips and best practices

- Create a readme.md that describes code conventions, branch strategies and a description of different modules in the git repo.
- Add a .gitignore file to avoid accidental commits of binaries or IDE workspace metadata.

Commit quality tips

- Make sure a commit contains related changes. A new feature and a seperate bugfix should not be in the same commit!
- Be descriptive in your commit messages.
- Mention workitem id's in your commit messages (ie, Jira issue ID's)
- Commit often
- Don't commit non-compiling clutter
  - Run tests before each commit
- Try to put only easily diff-able content into git like code or text (no jar files, zip files, iso files, pptx etc...)
    - Keeps the repository size down
    - Easier to see what changed
    - Track references to external binary files you need (ie. artifactory / nexus)

Branching tips

- If you have a long running branch, update your branch regularly with the changes from master!
- Tag released versions with version numbers for easy referencing
- Use a readme.md and describe the branching strategy you decided on with your team!
- When you do a `git pull`, make sure you include the --rebase flag to keep the history of a branch tidy.

## Cheatsheets

Cheatsheets are reference cards you can print and keep on your desk

- <https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet>
- <https://www.cloudways.com/blog/git-cheat-sheet/>
- <https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf>
- <https://www.git-tower.com/blog/git-cheat-sheet/>

## Git resources

- <https://git-scm.com/book/en/v2>
- <http://learngitbranching.js.org/>

## Interesting links per topic

### Branching strategies

- <https://www.atlassian.com/git/tutorials/comparing-workflows>

#### Gitflow branching strategy:

- <http://nvie.com/posts/a-successful-git-branching-model/>
- <https://datasift.github.io/gitflow/IntroducingGitFlow.html>
