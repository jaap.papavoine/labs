# Git module - Labs

## Exercise 1: Anatomy of a Repository

In this exercise we install git and create a repository.

1. Install Git
2. Create a repository
    - Open Git Bash
    - cd c:
    - mkdir git
    - cd git
    - git init demo
    - cd demo
3. Change the configuration
    - git config --global user.email "…"
    - git config --global user.name "…"
4. Run git status. The result should be that there are no commits.

## Exercise 2 - Add your first change

1.  Add a file to the git directory you created in Exercise 1. Use any text editor to give it some content.
2.  Add the file to the git index.
    - run 'git status' an notice how there are untracked changes.
    - git add myfile.txt
    - run 'git status' again and notice how the changes are now tracked. This means they are marked to be included in the next commit!
3.  Create your first commit!
    - git commit -m "Hurray, my first git commit!"
    - run 'git status' again and see now that there are no more changes (untracked, or tracked)

## Exercise 3 - Create your first branch

Until now, we have worked on the default 'master' branch. We want to create a new branch to isolate several changes (in example, for a new feature). We will call this branch 'mynewfeature'.

1.  Review that there currently is only one branch, called 'master.
    - git branch
2. Create a new branch named 'mynewfeature'.
    - git branch mynewfeature
3.  Review that the new branch shows up
    - git branch
    Note that the asterisk is still with master, that means that you are currently 'on' master, and any new commits will be added to master, and not your new branch.
4.  To fix this, we will do a 'checkout' of the new branch.
    - git checkout mynewfeature
5. Using the steps from exercise 2, create a new commit on mynewfeature branch.
    - Change, add or remove a file.
    - git add the new file
    - git commit -m "Some message"
    - Create another commit using the same steps.
6.  Use 'git log' to see the chain of commits in your branch.
    - git log
7.  Let's go and see if the master branch is indeed untouched.
    - git checkout master
    - Review the file, note how your changes have dissappeared!
    - Run 'git log' and notice how it does not show your commits on the mynewfeature branch.
    - Create a new commit (change, add, commit).
8. Let's get an overview of commits per branch
    - git show-branch
9. Done!

## Exercise 4 - Merge

1. Setup two different branches, both with their own unique commits. For instance a branch named 'branchA' and a branch named 'branchB'.
2. While 'in' branch A, try to merge branch B onto it.
    - git checkout branchA
    - git merge branchB
3. Check to see if the changes from branchB are now also visible onto branchA.

Bonus: Try to create a conflict!

    - Resolve the conflict using a text editor.
    - git add the resolved files.
    - git commit

## Exercise 5 - Rebase

1. Setup two different branches, both with their own unique commits. For instance a branch named 'branchA' and a branch named 'branchB'.
2. While 'in' branch A, try to rebase it onto branchB
    - git checkout branchA
    - git rebase branchB
3. Check to see if the changes from branchB are now also visible onto branchA.

Bonus: Try to create a conflict!

    - Resolve the conflict using a text editor.
    - git add the resolved files.
    - git commit

## Exercise 6 - Tags

Tagging is done with the `git tag` command. Try adding a tag to a previous commit.

1. Find a commit hash using the git log command.
2. Add a tag with the following command:
    - `git tag <yourtagname> <commithash>`
    - for example:
        - `git tag release-1.2.1 47a9bd`
    Note how you can use the first couple of characters for the commit hash. As long as the characters are unique, git will figure out what the full hash is.
3. Checkout your commit using the tagname.
    - git checkout release-1.2.1
    Note how your working directory has now changed to reflect the commit you tagged. This makes reverting to previous versions easier.
4. List all tags with the `git tag` command
